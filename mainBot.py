import vk
import random
import database

class MyBot:
    def __init__(self, token):
        self.session = vk.Session(access_token = token) 
        self.api = vk.API(self.session)
        self.db = database.database()
        self.users = self.db.get_users()
        self.messages_loop()
    
    def messages_loop(self):
        while True:
            dialogs = self.api.messages.getDialogs(unanswered = 1, v=5.92)
            if dialogs['count'] != 0:
                user_id = dialogs['items'][0]['message']['user_id']
                self.check_users(user_id)
                message = dialogs['items'][0]['message']['body']
                ans, attachment  = self.edit_message(message)
                if ans == False:
                    ans = 'Я тебя не понимаю'
                self.send_message(ans, user_id, attachment)

    def check_users(self, user_id):
        user = self.db.get_user(user_id)
        if user == False:
            name = self.api.users.get(user_ids=user_id, v=5.92)[0]['first_name']
            print(self.db.add_user(user_id, name))
 
    def send_message(self, message, user_id, attachment):
        try:
            if attachment == "True":
                self.api.messages.send(user_id=user_id, message = '', v=5.92, attachment=message, random_id=random.randint(0, 1000000))
            else:
                self.api.messages.send(user_id=user_id, message = message, v=5.92, random_id=random.randint(0, 1000000))
            return True
        except Exception:
            return False

    def edit_message(self, message):
        ans, attachment = self.db.get_ans(message) 
        if ans != False:
            return ans, attachment
        else:
            return False, False
        

        
bot = MyBot(token = 'f86482a7b1a846a6f56cdc21f19c7bab23273764b53fb3d76ae4f44e97d202d78ec710bf5f62b9bcf04c2')
